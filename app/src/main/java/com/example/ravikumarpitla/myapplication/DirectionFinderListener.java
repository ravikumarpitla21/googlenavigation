package com.example.ravikumarpitla.myapplication;

import com.example.ravikumarpitla.myapplication.models.Route;

import java.util.List;




public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
